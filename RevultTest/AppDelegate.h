//
//  AppDelegate.h
//  RevultTest
//
//  Created by Владислав Рябов on 23.08.16.
//  Copyright © 2016 Ryabov Vladislav. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

