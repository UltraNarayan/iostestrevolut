//
//  UIScrollView + calculateContentSize.h
//  RevultTest
//
//  Created by Рябов Владислав on 21.05.17.
//  Copyright © 2017 Ryabov Vladislav. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView(calculateContentSize)

-(void)calculateContentSize;

@end
