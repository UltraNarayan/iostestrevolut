//
//  RVCurrency.h
//  CConverter
//
//  Created by Владислав Рябов on 13.12.14.
//  Copyright © 2014 Ryabov Vladislav. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface RVCurrency : NSObject

@property NSString* stringCode;
@property NSNumber* nominal;
@property NSNumber* currencyValue;

@end
