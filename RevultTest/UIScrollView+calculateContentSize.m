//
//  UIScrollView + calculateContentSize.m
//  RevultTest
//
//  Created by Рябов Владислав on 21.05.17.
//  Copyright © 2017 Ryabov Vladislav. All rights reserved.
//

#import "UIScrollView+calculateContentSize.h"

@implementation UIScrollView(calculateContentSize)

-(void)calculateContentSize
{
    CGRect contentRect = CGRectZero;
    for (UIView *view in self.subviews) {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    self.contentSize = contentRect.size;
}

@end
