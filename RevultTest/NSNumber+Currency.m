//
//  NSNumber+RVStringFormat.m
//  RevultTest
//
//  Created by Владислав Рябов on 28.08.16.
//  Copyright © 2016 Ryabov Vladislav. All rights reserved.
//

#import "NSNumber+Currency.h"

@implementation NSNumber(Currency)

-(NSString *)stringForCurrencyCode:(NSString *)currencyCode;
{
    return [self stringForCurrencyCode:currencyCode fraction:2];
}

-(NSString *)stringForCurrencyCode:(NSString *)currencyCode fraction:(NSUInteger)fraction;
{
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    
    [formatter setLocale:[NSLocale currentLocale]];
    
    formatter.usesGroupingSeparator = false;

    if (currencyCode) {
        formatter.numberStyle = kCFNumberFormatterCurrencyStyle;
        formatter.currencyCode = currencyCode;
    } else {
        formatter.numberStyle = kCFNumberFormatterDecimalStyle;
    }
    
    if (trunc(self.floatValue) == self.floatValue) {
        [formatter setMaximumFractionDigits:0];
    } else {
        [formatter setMaximumFractionDigits:fraction];
    }
    
    return [formatter stringFromNumber:self];
}

@end
