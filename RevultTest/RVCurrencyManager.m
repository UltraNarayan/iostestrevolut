//
//  RVCurrencyManager.m
//  CConverter
//
//  Created by Владислав Рябов on 13.12.14.
//  Copyright © 2014 Ryabov Vladislav. All rights reserved.
//

#import "RVCurrencyManager.h"
#import "RVCurrency.h"

static RVCurrencyManager* __sharedManager;

@implementation RVCurrencyManager

// Singletone
// [RVCurrencyManager sharedManger]
+(RVCurrencyManager*)sharedManager{
    if (!__sharedManager) {
        __sharedManager = [RVCurrencyManager new];
    }
    return __sharedManager;
}


#pragma mark Public interface



-(void)loadCurrenciesFromLocalResource{
    // If file with currencies does not exist
    // it means that currencies was never loaded from internet
    // and we need to use blank
    if (![[NSFileManager defaultManager] fileExistsAtPath:[self pathForXMLFile]]) {
        // Recieving file oath inside app bundle
        NSString* pathToInternalFile = [[NSBundle mainBundle] pathForResource:@"currencies" ofType:@"xml"];
        
        // Copying it to sandbox
        // to load it later
        [[NSFileManager defaultManager] copyItemAtPath:pathToInternalFile
                                                toPath:[self pathForXMLFile]
                                                 error:nil];
    }
    
    // Loading file in to string to processing it
    //  with XML parser
    NSData *xmlData = [NSData dataWithContentsOfFile:[self pathForXMLFile]];

    [self loadCurrenciesFromXMLData:xmlData];
}

-(void)loadCurrenciesFromInternetWithCompletionBlock:(void(^)(BOOL isSuccess))completionBlock{
    // Forming loading URL
    NSURL* url = [NSURL URLWithString:@"http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml"];
    
    NSURLRequest* request = [NSURLRequest requestWithURL:url];
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {

        //Checking for Error
        if (error != nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completionBlock(false);
            });
            return;
        }
                               
        // Writing recieved data to file
        [data writeToFile:[self pathForXMLFile]
               atomically:YES];
        
        // Starting parser
        [self loadCurrenciesFromXMLData:data];
        
        // Call block if it's not nill
        // and could be executed
        if (completionBlock) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completionBlock(true); // Block will be executed on the main thread
            });
        }
        
    }] resume];
}

-(NSNumber*)convertFromCurrency:(RVCurrency*)fromCurrency
                     toCurrency:(RVCurrency*)toCurrency
                     withAmount:(NSNumber*)amount{
    
    double amountC = amount.doubleValue;
    
    // Calculating rate
    double k = [self rateFromCurrency:fromCurrency toCurrecy:toCurrency].doubleValue;
    
    return @(k * amountC);
}

-(NSNumber *)rateFromCurrency:(RVCurrency *)fromCurrency
                    toCurrecy:(RVCurrency *)toCurrency
{
    double fromCurrencyNominal = fromCurrency.nominal.doubleValue;
    double toCurrencyNominal = toCurrency.nominal.doubleValue;
    
    double fromCurrencyValue = fromCurrency.currencyValue.doubleValue;
    double toCurrencyValue = toCurrency.currencyValue.doubleValue;
    
    double k =  (fromCurrencyNominal * toCurrencyValue)/(toCurrencyNominal * fromCurrencyValue);
        
    k = roundf(k * 10000.0) / 10000.0;
    
    return @(k);
}

-(RVCurrency *)getCurrecyForStringCode:(NSString *)currencyCode
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"stringCode ==[c] %@",currencyCode];
    NSArray *array = [self.currencies filteredArrayUsingPredicate:predicate];
    return [array firstObject];
}

#pragma mark Private interface

-(void)loadCurrenciesFromXMLData:(NSData*)xmlData{

    //Adding base currency
    RVCurrency *currency = [RVCurrency new];
    currency.stringCode = @"EUR";
    currency.nominal = @(1);
    currency.currencyValue = @(1);
    
    // Initializing array of currencies
    self.currencies = [NSArray arrayWithObject:currency];
    
    
    
    // Initializing XML parser
    _parser = [[NSXMLParser alloc] initWithData:xmlData];
    _parser.delegate = self;
    [_parser parse]; // Launching XML parsing
}

#pragma mark NSXMLParserDelegate
// Delegate method for opening tag
-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    if ([elementName.lowercaseString isEqualToString:@"cube"]) {
        
        NSString *currencyCode = attributeDict[@"currency"];
        NSString * rate = attributeDict[@"rate"];
        
        if (currencyCode != nil && rate != nil) {
            RVCurrency* currency = [RVCurrency new];
            currency.currencyValue = @(rate.doubleValue);
            currency.stringCode = currencyCode;
            currency.nominal = @(1);
            
            self.currencies = [self.currencies arrayByAddingObject:currency];
        }
    }
}



// Delegate method for contents
-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
}

// Delegate method for closing tag
-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary*)attributes{

}

//Returns full path for XML file with rates
-(NSString*)pathForXMLFile{
    return [@"~/Library/currencies.xml" stringByExpandingTildeInPath];
}

@end












