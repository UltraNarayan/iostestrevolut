//
//  ViewController.m
//  RevultTest
//
//  Created by Владислав Рябов on 23.08.16.
//  Copyright © 2016 Ryabov Vladislav. All rights reserved.
//

#import "ViewController.h"
#import "RVCurrencyManager.h"
#import "RVCurrency.h"
#import "NSNumber+Currency.h"
#import "RVExchangeView.h"
#import "UIScrollView+calculateContentSize.h"
#import "NSString+Currency.h"
#import "RVScrollView.h"

#define kRefreshRate 30.0
#define kMaxNumberOfDigits 7
#define kMaxFractionDigits 2

@interface ViewController () <UITextFieldDelegate, UIScrollViewDelegate>

#pragma mark Outlets
@property (strong, nonatomic) IBOutlet RVScrollView *exchangeFromScrollView;
@property (strong, nonatomic) IBOutlet RVScrollView *exchangeToScrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *exchangeFromPageControl;
@property (strong, nonatomic) IBOutlet UIPageControl *exchangeToPageControl;
@property (strong, nonatomic) IBOutlet UILabel *rateLabel;
@property (strong, nonatomic) IBOutlet UIButton *exchangeButton;

#pragma mark Collections
@property (strong, nonatomic) NSMutableDictionary <NSString *,NSNumber *> *wallet;
@property (strong, nonatomic) NSArray <NSString*> *currencyCodes;
@property (strong, nonatomic) NSArray <RVCurrency*> *currencies;
@property (strong, nonatomic) NSArray <RVExchangeView *> *exchangeFromViews;
@property (strong, nonatomic) NSArray <RVExchangeView *> *exchangeToViews;


#pragma mark Variables
@property (assign, nonatomic) BOOL isEditingFromTextField;
@property (assign, nonatomic) NSInteger exchangeFromCurrentIndex;
@property (assign, nonatomic) NSInteger exchangeToCurrentIndex;
@property (strong, nonatomic) NSNumber *fromExchangeAmount;
@property (strong, nonatomic) NSNumber *toExchangeAmount;
@property (weak, nonatomic) NSTimer *timer;
@property (weak, nonatomic) UITextField *fromAmmountTextfield;
@property (weak, nonatomic) UITextField *toAmmountTextfield;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Initialization
    self.currencyCodes = @[@"USD",@"GBP",@"EUR"];
    self.wallet = @{@"USD":@(100),@"GBP":@(100),@"EUR":@(100)}.mutableCopy;
    self.exchangeFromCurrentIndex = 0;
    self.exchangeToCurrentIndex = 1;
    self.fromExchangeAmount = @(10);
    self.exchangeButton.enabled = NO;
    self.isEditingFromTextField = YES;
    
    self.exchangeToScrollView.delegate = self;
    self.exchangeFromScrollView.delegate = self;
    
    [[RVCurrencyManager sharedManager] loadCurrenciesFromLocalResource];
    
    [self populateCurrenciesArray];
    
    self.toExchangeAmount = [[RVCurrencyManager sharedManager] convertFromCurrency:_currencies[_exchangeFromCurrentIndex]
                                                                        toCurrency:_currencies[_exchangeToCurrentIndex]
                                                                        withAmount:_fromExchangeAmount];
    
    self.exchangeFromViews = [self prepareExchangeViewsForScrollView:self.exchangeFromScrollView];
    self.exchangeToViews = [self prepareExchangeViewsForScrollView:self.exchangeToScrollView];
    
    [self updateExchangeViews];
    [self updateRateLabel];
    [self updateExchangeButton];
    [self updateFirstResponder];
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:kRefreshRate target:self selector:@selector(fetchData) userInfo:nil repeats:YES];
}


#pragma mark Private interface

- (void)populateCurrenciesArray
{
    NSMutableArray *currencies = [NSMutableArray new];
    [self.currencyCodes enumerateObjectsUsingBlock:^(NSString*  _Nonnull code, NSUInteger idx, BOOL * _Nonnull stop) {
        [currencies addObject:[[RVCurrencyManager sharedManager] getCurrecyForStringCode:code]];
    }];
    self.currencies = currencies;
}

- (void)updateRateLabel
{
    RVCurrency *fromCurrencie = self.currencies[self.exchangeFromCurrentIndex];
    RVCurrency *toCurrencie= self.currencies[self.exchangeToCurrentIndex];
    
    NSNumber *rate = [[RVCurrencyManager sharedManager] rateFromCurrency:fromCurrencie
                                                               toCurrecy:toCurrencie];
    
    NSString *rateString = [NSString rateStringWithLeftCurrency:fromCurrencie
                                                  rightCurrency:toCurrencie
                                                           rate:rate
                                                       fraction: 4];
    
    self.rateLabel.text = rateString;
}

-(NSArray *)prepareExchangeViewsForScrollView:(UIScrollView *)scrollView
{
    NSMutableArray *views = [NSMutableArray new];

    [self.currencies enumerateObjectsUsingBlock:^(RVCurrency * _Nonnull currencie, NSUInteger idx, BOOL * _Nonnull stop) {
        
        RVExchangeView *exchangeView = [[NSBundle mainBundle] loadNibNamed:@"RVExchangeView" owner:nil options:nil][0];
        
        CGRect parentRect = scrollView.bounds;
        
        exchangeView.frame = CGRectMake(idx * parentRect.size.width, 0, parentRect.size.width, parentRect.size.height);
        
        exchangeView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        
        [scrollView addSubview:exchangeView];
        
        [views addObject:exchangeView];
        
    }];
    return views;
}

-(void)viewDidLayoutSubviews
{
    //Configuring scroll views content size
    [self.exchangeToScrollView calculateContentSize];
    [self.exchangeFromScrollView calculateContentSize];
    
    CGFloat width = self.exchangeFromScrollView.frame.size.width;
    self.exchangeToScrollView.contentOffset = CGPointMake(width * self.exchangeToCurrentIndex, 0);
    self.exchangeToPageControl.currentPage = self.exchangeToCurrentIndex;
    
    self.exchangeFromScrollView.contentOffset = CGPointMake(width * self.exchangeFromCurrentIndex, 0);
    self.exchangeFromPageControl.currentPage = self.exchangeFromCurrentIndex;
}

-(void)updateExchangeButton
{
    if (self.exchangeToCurrentIndex == self.exchangeFromCurrentIndex){
        self.exchangeButton.enabled = false;
        return;
    }
    
    RVCurrency *fromCurrency = _currencies[_exchangeFromCurrentIndex];
    
    if(_fromExchangeAmount.doubleValue > _wallet[fromCurrency.stringCode].doubleValue || _fromExchangeAmount.doubleValue == 0.0){
        self.exchangeButton.enabled = NO;
    } else {
        self.exchangeButton.enabled = YES;
    }
}

-(void)updateFirstResponder
{
    if (_isEditingFromTextField) {
        RVExchangeView *view = self.exchangeFromViews[_exchangeFromCurrentIndex];
        self.fromAmmountTextfield = view.amountOfChangeTextField;
        [self.fromAmmountTextfield becomeFirstResponder];
    } else {
        RVExchangeView *view = self.exchangeToViews[_exchangeToCurrentIndex];
        self.toAmmountTextfield = view.amountOfChangeTextField;
        [self.toAmmountTextfield becomeFirstResponder];
    }
}

-(void)updateExchangeViews
{
    [self.exchangeFromViews enumerateObjectsUsingBlock:^(RVExchangeView * _Nonnull view, NSUInteger idx, BOOL * _Nonnull stop) {

        RVCurrency *currency = self.currencies[idx];
        
        NSString *AmountString =  [self.wallet[currency.stringCode]  stringForCurrencyCode:currency.stringCode];
        
        view.amountLabel.text =  [NSString stringWithFormat: NSLocalizedString(@"You have %@",nil),AmountString];
        view.currencyCodeLabel.text = currency.stringCode;
        view.amountOfChangeTextField.delegate = self;
        view.rateLabel.text = @"";
        
        NSNumber *amount;
        
        if (self.isEditingFromTextField){
            amount = self.fromExchangeAmount;
        } else {
            
            RVCurrency *toCurrency = self.currencies[_exchangeToCurrentIndex];
            amount = [[RVCurrencyManager sharedManager] convertFromCurrency:toCurrency toCurrency:currency withAmount:self.toExchangeAmount];
        }
        
        view.amountOfChangeTextField.text = [NSString stringWithFormat:@"-%@", [amount stringForCurrencyCode:nil]];
        
        RVCurrency *fromCurrency = _currencies[_exchangeFromCurrentIndex];
        
        if(_fromExchangeAmount.doubleValue > _wallet[fromCurrency.stringCode].doubleValue){
            view.amountLabel.textColor = [UIColor redColor];
        } else {
            view.amountLabel.textColor = [UIColor blackColor];
        }
        
        if (idx == _exchangeFromCurrentIndex){
            self.fromAmmountTextfield = view.amountOfChangeTextField;
        }
    }];
    
    [self.exchangeToViews enumerateObjectsUsingBlock:^(RVExchangeView * _Nonnull view, NSUInteger idx, BOOL * _Nonnull stop) {

        RVCurrency *currency = self.currencies[idx];
        
        NSString *AmountString =  [self.wallet[currency.stringCode]  stringForCurrencyCode:currency.stringCode];
        
        view.amountLabel.text =  [NSString stringWithFormat: NSLocalizedString(@"You have %@",nil),AmountString];
        view.currencyCodeLabel.text = currency.stringCode;
        view.amountOfChangeTextField.delegate = self;
        view.rateLabel.text = @"";
        
        RVCurrency *fromCurrency = self.currencies[self.exchangeFromCurrentIndex];
        
        NSNumber *rate = [[RVCurrencyManager sharedManager] rateFromCurrency:currency toCurrecy:fromCurrency];
        view.rateLabel.text = [NSString rateStringWithLeftCurrency:currency rightCurrency:fromCurrency rate:rate];
        
        NSNumber *amount;
        
        if (self.isEditingFromTextField) {
            RVCurrency *fromCurrency = self.currencies[_exchangeFromCurrentIndex];
            
            amount = [[RVCurrencyManager sharedManager] convertFromCurrency:fromCurrency
                                                                 toCurrency:currency
                                                                 withAmount:self.fromExchangeAmount];
        } else {
            amount = self.toExchangeAmount;
        }
        
        view.amountOfChangeTextField.text = [NSString stringWithFormat:@"+%@",[amount stringForCurrencyCode:nil]];
        
        if (idx == _exchangeToCurrentIndex){
            self.toAmmountTextfield = view.amountOfChangeTextField;
        }
    }];
}

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.exchangeToScrollView){
        
        self.exchangeToCurrentIndex = self.exchangeToScrollView.contentOffset.x / self.exchangeToScrollView.bounds.size.width;
    }
    if (scrollView == self.exchangeFromScrollView) {
        self.exchangeFromCurrentIndex = self.exchangeFromScrollView.contentOffset.x / self.exchangeFromScrollView.bounds.size.width;
    }
    [self updateExchangeViews];
    [self updateRateLabel];
    [self updateExchangeButton];
    [self updateFirstResponder];
}

#pragma mark UITextFieldDelegate

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    formatter.locale = [NSLocale currentLocale];
    
    
    if (textField == self.fromAmmountTextfield){
        _isEditingFromTextField = YES;

        NSString *numberString = [self.fromAmmountTextfield.text stringByReplacingOccurrencesOfString:@"-" withString:@""];
        NSNumber *amount = [formatter numberFromString:numberString];
        
        if (amount == nil){
            amount = @(0);
        }
        
        self.fromExchangeAmount = amount;
        [self updateFromTextFieldsWithString:textField.text];
        
    } else if (textField == self.toAmmountTextfield){
        _isEditingFromTextField = NO;
        NSString *numberString = [self.toAmmountTextfield.text stringByReplacingOccurrencesOfString:@"+" withString:@""];
        NSNumber *amount = [formatter numberFromString:numberString];
        
        if (amount == nil){
            amount = @(0);
        }
        self.toExchangeAmount = amount;
        [self updateToTextFieldsWithString:textField.text];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //Check if replacing string contains only numbers and decimals
    NSCharacterSet* decimalSet = [NSCharacterSet characterSetWithCharactersInString:@"0987654321.,"];
    NSCharacterSet* stringSet = [NSCharacterSet characterSetWithCharactersInString:string];
    if (![decimalSet isSupersetOfSet:stringSet]){
        return NO;
    }
    
    NSString *text = textField.text;
    //Check if result string will contain only one dot symbol
    if ([string containsString:@"."] && [[text stringByReplacingCharactersInRange:range withString:@""] containsString:@"."]){
        return NO;
    }
    
    text = [text stringByReplacingCharactersInRange:range withString:string];
    
    //Check if result string contain + or -
    if (textField == self.fromAmmountTextfield && ![text containsString:@"-"]){
        return  NO;
    }
    if (textField == self.toAmmountTextfield && ![text containsString:@"+"]){
        return  NO;
    }
    
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    formatter.locale = [NSLocale currentLocale];
    
    
    NSArray<NSString *> *components = [text componentsSeparatedByString:formatter.decimalSeparator];
    
    //No more than 2 digits after dot
    if (components.count >= 2) {
        if (components[1].length > kMaxFractionDigits) {
            return NO;
        }
    }
    
    if (components.count >= 1) {
        if (components[0].length > kMaxNumberOfDigits){
            return NO;
        }
    }
    
    if (components.count == 2 && [string isEqualToString:@"0"]) {
        return YES;
    }
    
    if ([string isEqual:formatter.decimalSeparator]) {
        return YES;
    }
    
    if (textField == self.fromAmmountTextfield){
        //Set new from ammount
        
        NSNumber *number = [formatter numberFromString:[text stringByReplacingOccurrencesOfString:@"-" withString:@""]];
        if (number == nil) {
            number = @(0);
        }
        
        self.fromExchangeAmount = number;
        //Convert currency
        self.toExchangeAmount = [[RVCurrencyManager sharedManager] convertFromCurrency:_currencies[_exchangeFromCurrentIndex] toCurrency:_currencies[_exchangeToCurrentIndex] withAmount:self.fromExchangeAmount];
        
    } else {
        //Set new to ammount
        NSNumber *number = [formatter numberFromString:[text stringByReplacingOccurrencesOfString:@"+" withString:@""]];
        if (number == nil) {
            number = @(0);
        }
        self.toExchangeAmount = number;
        //Convert currency
        self.fromExchangeAmount = [[RVCurrencyManager sharedManager] convertFromCurrency:_currencies[_exchangeToCurrentIndex]
                                                                              toCurrency:_currencies[_exchangeFromCurrentIndex]
                                                                              withAmount:_toExchangeAmount];
    }
    
    [self updateExchangeViews];
    [self updateExchangeButton];
    
    return  NO;
}

-(void)updateFromTextFieldsWithString:(NSString *)string
{
    [self.exchangeFromViews enumerateObjectsUsingBlock:^(RVExchangeView * _Nonnull view, NSUInteger idx, BOOL * _Nonnull stop) {
        view.amountOfChangeTextField.text = string;
    }];
}

-(void)updateToTextFieldsWithString:(NSString *)string
{
    [self.exchangeToViews enumerateObjectsUsingBlock:^(RVExchangeView * _Nonnull view, NSUInteger idx, BOOL * _Nonnull stop) {
        view.amountOfChangeTextField.text = string;
    }];
}

- (void)fetchData
{
    NSLog(@"Starting fetch");
    //Loading rates
    [[RVCurrencyManager sharedManager] loadCurrenciesFromInternetWithCompletionBlock:^(BOOL isSuccess){
        
        if (isSuccess == false){
            NSLog(@"Data fetch failed");
            return;
        }
        
        [self populateCurrenciesArray];
        
        if (self.isEditingFromTextField) {
            self.toExchangeAmount = [[RVCurrencyManager sharedManager] convertFromCurrency:_currencies[_exchangeFromCurrentIndex]
                                                                                toCurrency:_currencies[_exchangeToCurrentIndex]
                                                                                withAmount:_fromExchangeAmount];
        } else {
            self.fromExchangeAmount = [[RVCurrencyManager sharedManager] convertFromCurrency:_currencies[_exchangeToCurrentIndex]
                                                                                  toCurrency:_currencies[_exchangeFromCurrentIndex]
                                                                                  withAmount:_toExchangeAmount];
        }
        

        [self updateRateLabel];
        [self updateExchangeViews];
        [self updateExchangeButton];
        
        NSLog(@"Data fetched");
    }];
}



#pragma mark Actions

- (IBAction)exchangeAction:(id)sender {
    
    RVCurrency *fromCurrencie = _currencies[_exchangeFromCurrentIndex];
    RVCurrency *toCurrencie = _currencies[_exchangeToCurrentIndex];
    
    //Check if currencies are different
    if([fromCurrencie.stringCode isEqualToString:toCurrencie.stringCode]){
        NSLog(@"Cant exchange matching currencies.");
        return;
    }
    
    //Check if we have enough currency
    NSNumber *currentFromCurrencyAmount = self.wallet[fromCurrencie.stringCode];
    if(_fromExchangeAmount.doubleValue > currentFromCurrencyAmount.doubleValue){
        NSLog(@"Cant exchange. Not enough currency.");
        return;
    }
    
    NSNumber *toCurrencyAmount = self.wallet[toCurrencie.stringCode];
    
    [self.wallet setObject:@(currentFromCurrencyAmount.doubleValue - _fromExchangeAmount.doubleValue) forKey:fromCurrencie.stringCode];
    
    [self.wallet setObject:@(toCurrencyAmount.doubleValue + _toExchangeAmount.doubleValue) forKey:toCurrencie.stringCode];
    
    [self updateExchangeViews];
    [self updateExchangeButton];
}

@end
