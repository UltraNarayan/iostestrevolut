//
//  NSNumber+RVStringFormat.h
//  RevultTest
//
//  Created by Владислав Рябов on 28.08.16.
//  Copyright © 2016 Ryabov Vladislav. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber(Currency)
/*!
 Returns number as formeted string for currency. Example: £1435.67
 */
-(NSString *)stringForCurrencyCode:(NSString *)currencyCode;

-(NSString *)stringForCurrencyCode:(NSString *)currencyCode fraction:(NSUInteger)fraction;

@end
