//
//  NSString+Currency.m
//  RevultTest
//
//  Created by Рябов Владислав on 22.05.17.
//  Copyright © 2017 Ryabov Vladislav. All rights reserved.
//

#import "NSString+Currency.h"
#import "NSNumber+Currency.h"

@implementation NSString(Currency)

+ (NSString *)rateStringWithLeftCurrency:(RVCurrency *)leftCurrency
                           rightCurrency:(RVCurrency *)rightCurrency
                                    rate:(NSNumber *)rate
{
    return [self rateStringWithLeftCurrency:leftCurrency rightCurrency:rightCurrency rate:rate fraction:2];
}

+ (NSString *)rateStringWithLeftCurrency:(RVCurrency *)leftCurrency
                          rightCurrency:(RVCurrency *)rightCurrency
                                    rate:(NSNumber *)rate
                                fraction:(NSUInteger)fraction
{
    NSString *leftString = [leftCurrency.nominal stringForCurrencyCode:leftCurrency.stringCode];
    
    NSString *rightString = [rate stringForCurrencyCode:rightCurrency.stringCode fraction:fraction];
    
    return [NSString stringWithFormat:@"%@ = %@",leftString,rightString];
}



@end
