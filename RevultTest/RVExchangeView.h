//
//  RVExchangeCollectionViewCell.h
//  RevultTest
//
//  Created by Владислав Рябов on 24.08.16.
//  Copyright © 2016 Ryabov Vladislav. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RVExchangeView : UIView

@property (strong, nonatomic) IBOutlet UILabel *currencyCodeLabel;
@property (strong, nonatomic) IBOutlet UITextField *amountOfChangeTextField;
@property (strong, nonatomic) IBOutlet UILabel *rateLabel;
@property (strong, nonatomic) IBOutlet UILabel *amountLabel;

@end
