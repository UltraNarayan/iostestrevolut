//
//  RVCurrencyManager.h
//  CConverter
//
//  Created by Владислав Рябов on 13.12.14.
//  Copyright © 2014 Ryabov Vladislav. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RVCurrency;

@interface RVCurrencyManager : NSObject <NSXMLParserDelegate>{
    NSXMLParser* _parser;
}

+(RVCurrencyManager*)sharedManager;

#pragma mark Properties
@property NSArray* currencies;

#pragma mark Methods
-(void)loadCurrenciesFromLocalResource;
-(void)loadCurrenciesFromInternetWithCompletionBlock:(void(^)(BOOL isSuccess))completionBlock;

-(NSNumber*)convertFromCurrency:(RVCurrency*)fromCurrency
                     toCurrency:(RVCurrency*)toCurrency
                     withAmount:(NSNumber*)amount;

-(NSNumber *)rateFromCurrency:(RVCurrency *)fromCurrency
                    toCurrecy:(RVCurrency *)toCurrency;

-(RVCurrency *)getCurrecyForStringCode:(NSString *)currencyCode;

@end
