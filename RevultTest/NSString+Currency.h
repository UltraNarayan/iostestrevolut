//
//  NSString+Currency.h
//  RevultTest
//
//  Created by Рябов Владислав on 22.05.17.
//  Copyright © 2017 Ryabov Vladislav. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RVCurrency.h"

@interface NSString(Currency)

// Returns string as "$1 = £0.69"
+ (NSString *)rateStringWithLeftCurrency:(RVCurrency *)leftCurrency
                          rightCurrency:(RVCurrency *)rightCurrency
                                   rate:(NSNumber *)rate;

+ (NSString *)rateStringWithLeftCurrency:(RVCurrency *)leftCurrency
                           rightCurrency:(RVCurrency *)rightCurrency
                                    rate:(NSNumber *)rate
                                fraction:(NSUInteger)fraction;

@end
